import logger from './src/shared/logger';
import childProcess from 'child_process';
import fs from 'fs-extra';

(async () => {
  try {
    // Remove current build
    await remove('./dist/');
    await copy('./src/locale', './dist/locale');
    await exec('tsc --build tsconfig.prod.json', './');
  } catch (err) {
    logger.error('[Build] remove build', err);
  }
})();

function remove(loc: string): Promise<void> {
  return new Promise((res, rej) => {
    return fs.remove(loc, (err) => {
      return !!err ? rej(err) : res();
    });
  });
}

function copy(src: string, dest: string): Promise<void> {
  return new Promise((res, rej) => {
    return fs.copy(src, dest, (err) => {
      return !!err ? rej(err) : res();
    });
  });
}

function exec(cmd: string, loc: string): Promise<void> {
  return new Promise((res, rej) => {
    return childProcess.exec(cmd, { cwd: loc }, (err, stdout, stderr) => {
      if (!!stdout) {
        logger.info('[Build]', stdout);
      }
      if (!!stderr) {
        logger.warn('[Build]', stderr);
      }
      return !!err ? rej(err) : res();
    });
  });
}
