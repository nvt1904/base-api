FROM node:16-alpine as dev

WORKDIR /usr/src/app

COPY package*.json ./

RUN apk add --update python3 make g++ openssl && rm -rf /var/cache/apk/*

RUN yarn

RUN npx prisma migrate