import firebaseAdmin from 'firebase-admin';

// Initialize the app with a service account, granting admin privileges
firebaseAdmin.initializeApp();

export default firebaseAdmin;
