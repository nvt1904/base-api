import firebaseAdmin from '@service/firebase/configs/firebaseAdmin';

const sendMessageService = (
  message: firebaseAdmin.messaging.MulticastMessage
) => {
  firebaseAdmin.messaging().sendMulticast(message);
};

export default sendMessageService;
