import { prisma } from '@app';

export type QueryConversation = {
  userId: string;
  search?: string;
  take: number;
  skip: number;
  cursorId?: string;
};
const listConversationService = async (query: QueryConversation) => {
  const items = await prisma.conversation.findMany({
    where: { users: { some: { id: query.userId } } },
    select: {
      id: true,
      users: {
        select: {
          id: true,
          avatar: true,
          name: true,
          online: true
        }
      },
      message: {
        include: {
          files: true
        }
      },
      createdAt: true
    },
    skip: query.skip,
    take: query.take,
    orderBy: [{ updatedAt: 'desc' }],
    ...(query.cursorId ? { cursor: { id: query.cursorId } } : {})
  });
  const total = await prisma.conversation.count({
    where: { users: { some: { id: query.userId } } }
  });
  return { items, total };
};

export default listConversationService;
