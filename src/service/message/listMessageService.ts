import { prisma } from '@app';
import ArgumentError from '@shared/error/ArgumentError';

export type QueryListMessage = {
  userId: string;
  conversationId: string;
  take: number;
  skip: number;
  cursorId?: string;
};

const listMessageService = async (query: QueryListMessage) => {
  const conversation = await prisma.conversation.findFirst({
    where: { users: { some: { id: query.userId } } },
    include: {
      users: true
    }
  });
  if (conversation) {
    const items = await prisma.message.findMany({
      where: {
        conversationId: conversation.id
      },
      include: {
        files: true
      },
      skip: query.skip,
      take: query.take,
      orderBy: [{ createdAt: 'desc' }],
      ...(query.cursorId ? { cursor: { id: query.cursorId } } : {})
    });
    const total = await prisma.message.count({
      where: {
        conversationId: conversation.id
      }
    });
    return {
      items,
      total
    };
  }
  throw new ArgumentError();
};

export default listMessageService;
