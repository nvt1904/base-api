import { prisma } from '@app';
import { Conversation } from '@prisma/client';
import moveFileService from '@service/cloudinary/moveFileService';
import syncFileDBService from '@service/cloudinary/syncFileDBService';
import ArgumentError from '@shared/error/ArgumentError';
import { pick } from 'lodash';
import { io } from '@restful/server';

export type DataCreateMessage = {
  senderId: string;
  content: string;
  files?: string[];
  receiverId?: string;
  conversationId?: string;
};
const createMessageService = async (data: DataCreateMessage) => {
  const { senderId, content, files, receiverId, conversationId } = data;
  if (senderId === receiverId) {
    throw new ArgumentError();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let conversation: Conversation | any;
  if (conversationId) {
    conversation = await prisma.conversation.findFirst({
      where: {
        users: {
          some: {
            id: senderId
          }
        }
      },
      include: {
        users: true
      }
    });
    if (!conversation) {
      throw new ArgumentError();
    }
  }
  if (receiverId) {
    const receiver = await prisma.user.findFirst({
      where: {
        id: receiverId
      }
    });
    if (!receiver) {
      throw new ArgumentError();
    }
    const receiverIds = [senderId, receiver.id];
    conversation = await prisma.conversation.findFirst({
      where: {
        users: {
          every: {
            id: {
              in: receiverIds
            }
          }
        }
      },
      include: {
        users: true
      }
    });
    if (!conversation) {
      conversation = await prisma.conversation.create({
        data: {
          users: {
            connect: [{ id: senderId }, { id: receiverId }]
          }
        }
      });
    }
  }

  const newMessage = await prisma.message.create({
    data: {
      content,
      conversationId: conversation.id,
      userId: senderId
    }
  });
  if (files && files.length) {
    const moveAndSyncFile = (item: string): Promise<string> =>
      new Promise((resolve) =>
        moveFileService(item, 'message').then((info) =>
          syncFileDBService(info).then((file) => resolve(file.id))
        )
      );
    const fileIds: string[] = await Promise.all(files.map(moveAndSyncFile));
    if (fileIds && fileIds.length) {
      await prisma.message.update({
        where: {
          id: newMessage.id
        },
        data: {
          files: {
            connect: fileIds.map((id) => ({ id }))
          }
        }
      });
    }
  }
  updateLastMessageAndEmitMessage(newMessage.id);
  return { id: newMessage.id };
};

const updateLastMessageAndEmitMessage = (messageId: string) => {
  prisma.message
    .findFirst({
      where: { id: messageId },
      select: {
        id: true,
        content: true,
        files: true,
        userId: true,
        user: true,
        conversationId: true,
        conversation: {
          include: {
            users: true
          }
        }
      }
    })
    .then(async (message) => {
      if (message) {
        await prisma.conversation.update({
          where: { id: message.conversationId },
          data: { messageId: message.id }
        });
        message?.conversation?.users?.map((user) => {
          io.to(`private_user_${user.id}`).emit(
            'newMessage',
            pick(message, [
              'id',
              'userId',
              'user.avatar',
              'user.name',
              'conversationId',
              'conversation.id',
              'content',
              'files',
              'createdAt'
            ])
          );
        });
      }
    });
};

export default createMessageService;
