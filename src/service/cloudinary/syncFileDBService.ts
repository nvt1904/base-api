import { prisma } from '@app';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const syncFileDBService = async (info: any, replaceId?: string) => {
  const data = {
    name: info.original_filename || info.public_id.split('/').pop(),
    publicId: info.public_id,
    format: info.format,
    type: info.resource_type,
    bytes: info.bytes,
    url: info.url,
    access: info.access_mode
  };
  const file = await prisma.file.findFirst({
    where: {
      OR: [
        {
          publicId: data.publicId
        },
        {
          id: replaceId
        }
      ]
    }
  });
  if (file) {
    return await prisma.file.update({
      where: {
        id: file.id
      },
      data
    });
  } else {
    return await prisma.file.create({
      data
    });
  }
};

export default syncFileDBService;
