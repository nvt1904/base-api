import cloudinary from 'cloudinary';

const moveFileService = (publicId: string, folder: string) => {
  const name = publicId.split('/').pop();
  return cloudinary.v2.uploader.rename(publicId, `${folder}/${name}`, {
    invalidate: true,
    overwrite: true
  });
};

export default moveFileService;
