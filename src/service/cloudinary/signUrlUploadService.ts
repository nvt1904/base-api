import { strGenerate } from '@shared/helper';
import cloudinary from 'cloudinary';
import { format } from 'date-fns';
import slug from 'slug';

const signUrlUploadService = (name?: string) => {
  const fileName = `${format(new Date(), 'yyyyMMddHHmmss')}-${slug(
    name || strGenerate({ length: 10, lowerCase: true })
  )}`;
  return cloudinary.v2.utils.sign_request({
    public_id: `temp/${fileName}`,
    timestamp: Math.round(new Date().getTime() / 1000)
  });
};

export default signUrlUploadService;
