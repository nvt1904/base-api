import cloudinary from 'cloudinary';

const signUrlAccessService = (publicId?: string) => {
  return cloudinary.v2.utils.sign_request({
    public_id: publicId,
    sign_url: true,
    type: 'authenticated'
  });
};

export default signUrlAccessService;
