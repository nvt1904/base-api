import { strGenerate } from '@shared/helper';
import { Request } from 'express';
import s3 from './config/s3';

export type SignedUrlParams = {
  Bucket: string;
  Key: string;
  ACL?: string;
};

const signedUrlService = (req: Request) => {
  const params: SignedUrlParams = {
    Bucket: String(process.env.AWS_BUCKET_NAME),
    Key: strGenerate({ prefix: Date.now().toString() })
  };
  if (String(req.query?.operation) === 'putObject') {
    params.ACL = String(req.query?.acl) || 'public-read';
  }
  return s3.getSignedUrlPromise(String(req.query?.operation), params);
};

export default signedUrlService;
