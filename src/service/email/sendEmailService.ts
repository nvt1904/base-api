import logger from '@shared/logger';
import nodeMailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

export default (
  options: Mail.Options,
  callback: (err: Error | null, info: any) => void = (error) => {
    logger.error('[Email]', error);
  }
) => {
  try {
    const transporter = nodeMailer.createTransport({
      host: String(process.env.EMAIL_HOST),
      port: Number(process.env.EMAIL_PORT),
      secure: Number(process.env.EMAIL_PORT) === 645,
      auth: {
        user: process.env.EMAIL_ADDRESS,
        pass: process.env.EMAIL_PASSWORD
      }
    });
    return transporter.sendMail(options, callback);
  } catch (error) {
    logger.error('[Email]', error);
  }
};
