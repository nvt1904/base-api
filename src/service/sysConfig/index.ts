import { prisma } from '@app';

export const getConfig = async <Config = Record<string, unknown>>(
  key: string
) => {
  const sysConfigDB = await prisma.sysConfig.findFirst({ where: { key } });
  if (sysConfigDB) {
    return sysConfigDB.value as Config;
  }
  return undefined;
};

export const saveConfig = async (
  key: string,
  value: Record<string, unknown>
) => {
  let sysConfigDB = await getConfig(key);
  if (sysConfigDB) {
    sysConfigDB = await prisma.sysConfig.update({
      where: { key },
      data: {
        value: JSON.stringify(value)
      }
    });
  } else {
    sysConfigDB = await prisma.sysConfig.create({
      data: {
        key,
        value: JSON.stringify(value)
      }
    });
  }
  return sysConfigDB.value;
};
