import { prisma } from '@app';
import sendEmailService from '@service/email/sendEmailService';
import { createToken } from '@shared/jwt';
import bcrypt from 'bcrypt';

export type DataRegister = {
  name: string;
  email: string;
  password: string;
};
const registerService = (data: DataRegister) => {
  const { name, email, password } = data;
  return prisma.user
    .create({
      data: { name, email, password: bcrypt.hashSync(password, 10) }
    })
    .then((user) => {
      const token = createToken(
        { id: user.id, type: 'user' },
        Number(process.env.JWT_VERIFY_EMAIL_EXPIRES)
      );
      sendEmailService({
        to: user.email,
        text: token
      });
      return { id: user.id };
    });
};

export default registerService;
