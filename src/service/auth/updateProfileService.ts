import { prisma } from '@app';
import moveFileService from '@service/cloudinary/moveFileService';
import syncFileDBService from '@service/cloudinary/syncFileDBService';
import NotFound from '@shared/error/EntryNotFound';
import getProfileService from './getProfileService';

export type DataUpdateProfile = {
  avatar?: string;
  name?: string;
};
const updateProfileService = (id: string, data: DataUpdateProfile) => {
  return getProfileService(id).then(async (user) => {
    if (user) {
      let avatarFileId;
      if (data.avatar) {
        const info = await moveFileService(data.avatar, 'avatar');
        const file = await syncFileDBService(info, user?.avatar?.id);
        if (file) {
          avatarFileId = file.id;
        }
      }
      await prisma.user.update({
        where: {
          id: user.id
        },
        data: {
          name: data.name,
          avatarFileId
        }
      });
      return true;
    }
    throw new NotFound('profile_not_found');
  });
};

export default updateProfileService;
