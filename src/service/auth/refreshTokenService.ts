import { prisma } from '@app';
import { UserStatus } from '@prisma/client';
import EntryInactive from '@shared/error/EntryInactive';
import EntryNotFound from '@shared/error/EntryNotFound';
import Unauthorized from '@shared/error/Unauthorized';
import jwt, { config, createAccessToken } from '@shared/jwt';
import { Payload } from '@type';
import { pick } from 'lodash';

const refreshTokenService = async (refreshToken: string) => {
  let newPayload: Payload;
  try {
    const payload = jwt.verify(refreshToken, config.secretKey);
    newPayload = pick(payload, ['id', 'type']) as Payload;
  } catch (error) {
    throw new Unauthorized();
  }
  const user = await prisma.user.findFirst({
    where: {
      id: newPayload.id
    }
  });
  if (user) {
    if (user.status === UserStatus.active) {
      const { accessToken, expires } = createAccessToken(newPayload);
      return { accessToken, expires: expires.toISOString() };
    }
    throw new EntryInactive();
  } else {
    throw new EntryNotFound();
  }
};

export default refreshTokenService;
