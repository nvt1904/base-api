import { prisma } from '@app';
import { UserStatus } from '@prisma/client';
import ArgumentError from '@shared/error/ArgumentError';
import EntryInactive from '@shared/error/EntryInactive';
import { signJwt } from '@shared/jwt';
import { Payload } from '@type';
import bcrypt from 'bcrypt';

export type Credentials = {
  email: string;
  password: string;
};

const loginService = (credentials: Credentials) => {
  const { email, password } = credentials;
  return prisma.user
    .findFirst({
      where: { email },
      select: { id: true, password: true, status: true }
    })
    .then((user) => {
      if (user && bcrypt.compareSync(password, user?.password)) {
        if (user.status === UserStatus.active) {
          const payload: Payload = { id: user.id, type: 'user' };
          return signJwt(payload);
        }
        throw new EntryInactive();
      }
      throw new ArgumentError('email_or_password_invalid');
    });
};

export default loginService;
