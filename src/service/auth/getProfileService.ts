import { prisma } from '@app';

const getProfileService = (id?: string) => {
  return prisma.user.findFirst({
    where: { id: id },
    select: {
      id: true,
      avatar: true,
      name: true,
      email: true,
      status: true
    }
  });
};

export default getProfileService;
