import { createGraphqlServer } from '@graphql/server';
import i18next from '@locale/config/i18next';
import { PrismaClient } from '@prisma/client';
import { createRestfulServer } from '@restful/server';
import cors from 'cors';
// import jobStart from '@shared/job';
import express from 'express';
import 'express-async-errors';
import helmet from 'helmet';
import { createServer } from 'http';
import { init } from 'node-cache-redis';
import path from 'path';
import * as rfs from 'rotating-file-stream';

// node cache redis
init({
  name: 'base-api',
  redisOptions: {
    host: process.env.DB_REDIS_HOST || 'localhost',
    port: Number(process.env.DB_REDIS_PORT || 6379)
  },
  defaultTtlInS: Number(process.env.DEFAULT_CACHE_URL || 60 * 60 * 1000)
});

// prisma
declare const global: NodeJS.Global;
export const prisma = global.prisma || new PrismaClient();
if (process.env.NODE_ENV === 'development') global.prisma = prisma;

// basic
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// security
if (process.env.NODE_ENV === 'production') {
  app.use(helmet());
}

// cors
app.use(cors());

// public
app.use(express.static('public'));

// language
app.use(i18next);

app.use((req, res, next) => {
  req.i18n.changeLanguage(
    req.i18n.language.split('-').shift() || process.env.LANGUAGE_DEFAULT || 'vi'
  );
  return next();
});

// http server
const server = createServer(app);

// restful
createRestfulServer(app, server);

// graphql
createGraphqlServer(app, server);

rfs.createStream('temp.log', {
  size: '10M',
  interval: '1d',
  path: path.join(process.cwd(), 'logs/logger')
});
// job
// jobStart();

export default server;
