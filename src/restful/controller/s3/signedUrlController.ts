import signedUrlService from '@service/s3/signedUrlService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const signedUrlController = (req: Request, res: Response, next: NextFunction) =>
  signedUrlService(req)
    .then((url: string) => res.jsonApi(HttpStatus.OK, { signedUrl: url }))
    .catch((error: Error) => next(error));

export default signedUrlController;
