import listMessageService from '@service/message/listMessageService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const listMessageController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { conversationId, skip, take, cursorId } = req.query;
  return listMessageService({
    userId: String(req.auth?.id),
    conversationId: String(conversationId),
    skip: Number(skip),
    take: Number(take),
    cursorId: String(cursorId)
  })
    .then((result) => {
      return res.jsonApi(HttpStatus.OK, result);
    })
    .catch((error: Error) => next(error));
};

export default listMessageController;
