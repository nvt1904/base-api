import createMessageService from '@service/message/createMessageService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const createMessageController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { content, userId, files, conversationId } = req.body;
  return createMessageService({
    senderId: String(req.auth?.id),
    content,
    files,
    receiverId: userId,
    conversationId
  })
    .then((result) => {
      return res.jsonApi(HttpStatus.OK, result);
    })
    .catch((error: Error) => next(error));
};

export default createMessageController;
