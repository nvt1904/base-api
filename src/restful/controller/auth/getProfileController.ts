import getProfileService from '@service/auth/getProfileService';
import NotFound from '@shared/error/EntryNotFound';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const getProfileController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  return getProfileService(req?.auth?.id)
    .then((user) => {
      if (user) {
        return res.jsonApi(HttpStatus.OK, user);
      }
      throw new NotFound('profile_not_found');
    })
    .catch((error) => next(error));
};

export default getProfileController;
