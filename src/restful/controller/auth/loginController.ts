import loginService from '@service/auth/loginService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const loginController = (req: Request, res: Response, next: NextFunction) => {
  const { email, password } = req.body;
  return loginService({ email, password })
    .then((result) => {
      return res.jsonApi(HttpStatus.OK, result);
    })
    .catch((error) => next(error));
};

export default loginController;
