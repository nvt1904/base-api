import refreshTokenService from '@service/auth/refreshTokenService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const refreshTokenController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { refreshToken } = req.body;
    return res.jsonApi(HttpStatus.OK, await refreshTokenService(refreshToken));
  } catch (error) {
    next(error);
  }
};

export default refreshTokenController;
