import updateProfileService from '@service/auth/updateProfileService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const updateProfileController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { name, avatar } = req.body;
  return updateProfileService(String(req.auth?.id), { name, avatar })
    .then((result) => {
      return res.jsonApi(HttpStatus.OK, { success: result });
    })
    .catch((error) => next(error));
};

export default updateProfileController;
