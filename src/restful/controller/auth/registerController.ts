import registerService from '@service/auth/registerService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const registerController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { name, email, password } = req.body;
  return registerService({ name, email, password })
    .then((result) => {
      return res.jsonApi(HttpStatus.OK, result);
    })
    .catch((error) => next(error));
};

export default registerController;
