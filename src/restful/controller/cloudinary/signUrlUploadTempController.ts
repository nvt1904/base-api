import signUrlUploadService from '@service/cloudinary/signUrlUploadService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const signUrlUploadTempController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    res.jsonApi(
      HttpStatus.OK,
      signUrlUploadService(String(req.query.name || ''))
    );
  } catch (error) {
    next(error);
  }
};

export default signUrlUploadTempController;
