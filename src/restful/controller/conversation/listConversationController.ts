import listConversationService from '@service/conversation/listConversationService';
import { NextFunction, Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

const listConversationController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { search, skip, take, cursorId = '' } = req.query;
  return listConversationService({
    userId: String(req.auth?.id),
    search: String(search),
    skip: Number(skip),
    take: Number(take),
    cursorId: String(cursorId)
  })
    .then((result) => {
      return res.jsonApi(HttpStatus.OK, result);
    })
    .catch((error: Error) => next(error));
};

export default listConversationController;
