import signedUrlController from '@restful/controller/s3/signedUrlController';
import signedUrlRequest from '@restful/router/s3/signedUrlRequest';
import { Router } from 'express';

const s3Router = Router();

s3Router.get('/signed-url', signedUrlRequest, signedUrlController);

export default s3Router;
