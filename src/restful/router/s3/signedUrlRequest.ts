import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const signedUrlRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  return request(req, res, next, {
    data: req.query,
    rules: {
      operation: 'required|in:putObject,getObject',
      acl: 'required_if:operation,putObject|in:private,public-read'
    },
    attributes: {}
  });
};

export default signedUrlRequest;
