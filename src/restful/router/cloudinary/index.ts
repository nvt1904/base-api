import signUrlUploadTempController from '@restful/controller/cloudinary/signUrlUploadTempController';
import { Router } from 'express';

const cloudinaryRouter = Router();

cloudinaryRouter.get('/sign-url-upload-temp', signUrlUploadTempController);

export default cloudinaryRouter;
