import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const createMessageRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  request(req, res, next, {
    data: { ...req.body },
    rules: {
      content: 'sometimes|required|max:255',
      'files.*': 'sometimes|required|max:255',
      conversationId: 'required_without:userId|exists:Conversation,id',
      userId: 'required_without:conversationId|exists:User,id'
    }
  });

export default createMessageRequest;
