import createMessageController from '@restful/controller/message/createMessageController';
import listMessageController from '@restful/controller/message/listMessageController';
import authMiddleware from '@restful/middleware/authMiddleware';
import pagingMiddleware from '@restful/middleware/pagingMiddleware';
import { Router } from 'express';
import createMessageValidator from './createMessageValidator';
import listMessageValidator from './listMessageValidator';

const messageRouter = Router();

messageRouter.get(
  '/',
  authMiddleware,
  listMessageValidator,
  pagingMiddleware,
  listMessageController
);

messageRouter.post(
  '/',
  authMiddleware,
  createMessageValidator,
  createMessageController
);

export default messageRouter;
