import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const createMessageRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  request(req, res, next, {
    data: { ...req.query },
    rules: {
      conversationId: 'required|exists:Conversation,id',
      cursorId: 'sometimes',
      limit: 'sometimes|numeric|max:50'
    }
  });

export default createMessageRequest;
