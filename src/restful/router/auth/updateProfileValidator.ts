import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const updateProfileRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  request(req, res, next, {
    data: req.body,
    rules: {
      name: 'sometimes|required|max:255',
      email: `sometimes|required|max:255|unique:User,email,id != '${req.auth?.id}'`
    }
  });

export default updateProfileRequest;
