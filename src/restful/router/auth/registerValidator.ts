import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const registerRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  request(req, res, next, {
    data: req.body,
    rules: {
      name: 'required|max:255',
      email: 'required|email|max:255|unique:User,email',
      password: 'required|max:255'
    }
  });

export default registerRequest;
