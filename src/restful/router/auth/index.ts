import loginController from '@restful/controller/auth/loginController';
import getProfileController from '@restful/controller/auth/getProfileController';
import refreshTokenController from '@restful/controller/auth/refreshTokenController';
import registerController from '@restful/controller/auth/registerController';
import authMiddleware from '@restful/middleware/authMiddleware';
import loginValidator from '@restful/router/auth/loginValidator';
import { Router } from 'express';
import refreshTokenValidator from './refreshTokenValidator';
import registerValidator from './registerValidator';
import updateProfileController from '@restful/controller/auth/updateProfileController';
import updateProfileValidator from './updateProfileValidator';

const authRouter = Router();

authRouter.post('/register', registerValidator, registerController);
authRouter.post('/login', loginValidator, loginController);
authRouter.post(
  '/refresh-token',
  refreshTokenValidator,
  refreshTokenController
);
authRouter.get('/profile', authMiddleware, getProfileController);
authRouter.put(
  '/profile',
  authMiddleware,
  updateProfileValidator,
  updateProfileController
);

export default authRouter;
