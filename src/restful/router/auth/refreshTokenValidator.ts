import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const refreshTokenRequest = async (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  request(req, res, next, {
    data: req.body,
    rules: {
      refreshToken: 'required'
    }
  });

export default refreshTokenRequest;
