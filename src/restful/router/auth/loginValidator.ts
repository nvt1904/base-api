import request from '@restful/validator';
import { NextFunction, Request, Response } from 'express';

const loginRequest = async (req: Request, res: Response, next: NextFunction) =>
  request(req, res, next, {
    data: req.body,
    rules: {
      email: 'required',
      password: 'required'
    }
  });

export default loginRequest;
