import listConversationController from '@restful/controller/conversation/listConversationController';
import authMiddleware from '@restful/middleware/authMiddleware';
import pagingMiddleware from '@restful/middleware/pagingMiddleware';
import { Router } from 'express';

const conversationRouter = Router();

conversationRouter.get(
  '/',
  authMiddleware,
  pagingMiddleware,
  listConversationController
);

export default conversationRouter;
