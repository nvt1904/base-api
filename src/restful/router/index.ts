import NotFound from '@shared/error/NotFound';
import authRouter from '@restful/router/auth';
import { Router } from 'express';
import cloudinaryRouter from './cloudinary';
import conversationRouter from './conversation';
import messageRouter from './message';
import s3Router from './s3';

const router = Router();

router.use('/cloudinary', cloudinaryRouter);
router.use('/s3', s3Router);
router.use('/auth', authRouter);
router.use('/message', messageRouter);
router.use('/conversation', conversationRouter);
router.all('*', () => {
  throw new NotFound();
});

export default router;
