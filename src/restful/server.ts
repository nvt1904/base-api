import authSocketMiddleware from '@restful/middleware/authSocketMiddleware';
import apiRoutes from '@restful/router';
import { handleErrorApi } from '@shared/error';
import morgan from '@shared/logger/morgan';
import { createAdapter } from '@socket.io/redis-adapter';
import { Express } from 'express-serve-static-core';
import { Server } from 'http';
import { createClient } from 'redis';
import { Server as ServerSocket } from 'socket.io';
import baseMiddleware from './middleware/baseMiddleware';

export const io = new ServerSocket();

export const createRestfulServer = (app: Express, server: Server) => {
  // socket
  io.listen(server, {
    cors: { origin: '*' }
  });

  const redisHost = process.env.DB_REDIS_HOST || 'localhost';
  const redisPort = Number(process.env.DB_REDIS_PORT || 6379);
  const redisUrl = process.env.REDIS_URL || `redis://${redisHost}:${redisPort}`;
  const pubClient = createClient({
    url: redisUrl
  });
  const subClient = pubClient.duplicate();
  const adapter = createAdapter(pubClient, subClient);
  io.adapter(adapter);

  io.use(authSocketMiddleware);

  // api restful
  app.use('/api', morgan, baseMiddleware, apiRoutes, handleErrorApi);
};
