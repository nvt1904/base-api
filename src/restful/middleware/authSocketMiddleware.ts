import { prisma } from '@app';
import Unauthorized from '@shared/error/Unauthorized';
import jwt, { config } from '@shared/jwt';
import { Payload } from '@type';
import { pick } from 'lodash';
import { Socket } from 'socket.io';
import { ExtendedError } from 'socket.io/dist/namespace';

const authSocketMiddleware = async (
  socket: Socket,
  next: (err?: ExtendedError) => void
) => {
  try {
    const accessToken = String(
      socket.handshake.query?.accessToken || ''
    )?.trim();
    const payload = jwt.verify(accessToken, config.secretKey);
    const newPayload = pick(payload, ['id', 'type']) as Payload;
    socket.auth = newPayload;
    await prisma.user.update({
      where: {
        id: socket.auth.id
      },
      data: {
        online: true
      }
    });
    // Join user vào phòng của user
    socket.join(`private_user_${socket.auth.id}`);
    socket.emit('info', { message: `Joined private_user_${socket.auth.id}` });
    socket.on('disconnect', async () => {
      if (socket.auth && socket.rooms.size === 0) {
        await prisma.user.update({
          where: {
            id: socket.auth.id
          },
          data: {
            online: false
          }
        });
      }
    });
    return next();
  } catch (error) {
    socket.disconnect();
    return next(new Unauthorized());
  }
};

export default authSocketMiddleware;
