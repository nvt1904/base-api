import Unauthorized from '@shared/error/Unauthorized';
import jwt, { config } from '@shared/jwt';
import { Payload } from '@type';
import { NextFunction, Request, Response } from 'express';
import { pick } from 'lodash';

const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const accessToken = (req.headers.authorization?.toString() || '').trim();
    const payload = jwt.verify(accessToken, config.secretKey);
    const newPayload = pick(payload, ['id', 'type']) as Payload;
    req.auth = newPayload;
    next();
  } catch (error) {
    return next(new Unauthorized());
  }
};

export default authMiddleware;
