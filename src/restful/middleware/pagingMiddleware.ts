import { NextFunction, Request, Response } from 'express';

const pagingMiddleware = (req: Request, res: Response, next: NextFunction) => {
  // page limit
  if (
    req.query.limit === undefined ||
    isNaN(Number(req.query.limit)) ||
    Number(req.query.limit) < 0
  ) {
    req.query.limit = String(10);
  }
  if (
    req.query.page === undefined ||
    isNaN(Number(req.query.page)) ||
    Number(req.query.limit) < 0
  ) {
    req.query.page = String(1);
  }

  // cursor prisma
  if (req.query.cursorId === undefined || !!req.query.cursorId) {
    req.query.cursorId = '';
  }

  // skip take prisma
  req.query.skip = String(
    Number(req.query.limit) *
      (Number(req.query.page) - 1 <= 0 ? 0 : Number(req.query.page) - 1)
  );
  req.query.take = req.query.limit;

  // search q = ?
  req.query.q = String(req.query.q || '');

  return next();
};

export default pagingMiddleware;
