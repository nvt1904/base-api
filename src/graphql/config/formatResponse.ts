import { GraphQLContext } from '@type';
import { GraphQLRequestContext, GraphQLResponse } from 'apollo-server-types';

const formatResponse: (
  response: GraphQLResponse,
  reqCtx: GraphQLRequestContext
) => GraphQLResponse | null = (res, reqCtx) => {
  const ctx = reqCtx.context as GraphQLContext;
  if (res.errors) {
    const newRes = res;
    newRes.errors = res.errors.map((error) => {
      const newError = { ...error };
      newError.message = ctx.t(`error:${error.message}`);
      return newError;
    });
  }
  return res;
};

export default formatResponse;
