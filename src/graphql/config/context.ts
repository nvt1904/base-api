import { prisma } from '@app';
import { GraphQLContext } from '@type';
import { Context, ContextFunction } from 'apollo-server-core';

const context: Context | ContextFunction = ({ req }): GraphQLContext => {
  return { i18n: req.i18n, t: req.t, prisma, req };
};

export default context;
