import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { loadTypedefsSync } from '@graphql-tools/load';
import { mergeSchemas } from '@graphql-tools/schema';
import { resolvers } from '@graphql/schema';
import { PostCrudResolver, PostRelationsResolver } from '@prisma/graphql';
import { gql } from 'apollo-server-core';
import { applyMiddleware } from 'graphql-middleware';
import {
  DateTimeResolver,
  JSONResolver,
  JWTResolver,
  UUIDResolver
} from 'graphql-scalars';
import { merge } from 'lodash';
import path from 'path';
import { buildSchemaSync } from 'type-graphql';
import permissions from './permissions';

const typeDefs = [
  ...loadTypedefsSync(path.join(process.cwd(), './**/*.gql'), {
    loaders: [new GraphQLFileLoader()]
  }).map(
    (item) =>
      gql`
        ${item.rawSDL}
      `
  )
];

const schemaPrisma = buildSchemaSync({
  resolvers: [PostCrudResolver, PostRelationsResolver],
  validate: false
});

const schema = applyMiddleware(
  mergeSchemas({
    schemas: [schemaPrisma],
    typeDefs: typeDefs,
    resolvers: merge(resolvers, {
      DateTime: DateTimeResolver,
      JWT: JWTResolver,
      UUID: UUIDResolver,
      JSON: JSONResolver
    })
  }),
  permissions
);

export default schema;
