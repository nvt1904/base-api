import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';

const plugins = [ApolloServerPluginLandingPageGraphQLPlayground()];

export default plugins;
