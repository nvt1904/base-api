import context from '@graphql/config/context';
import formatError from '@graphql/config/formatError';
import formatResponse from '@graphql/config/formatResponse';
import plugins from '@graphql/config/plugins';
import { ApolloServer } from 'apollo-server-express';
import { Express } from 'express-serve-static-core';
import { execute, subscribe } from 'graphql';
import { PubSub } from 'graphql-subscriptions';
import { Server } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import schema from '@graphql/config/schema';
import { GraphQLContext } from '@type';
import logger from '@shared/logger';

export const pubsub = new PubSub();

export const createGraphqlServer = (app: Express, server: Server) => {
  const apolloServer = new ApolloServer<GraphQLContext>({
    schema,
    context,
    formatError,
    formatResponse,
    plugins,
    logger
  });

  apolloServer.start().then(() => {
    apolloServer.applyMiddleware({ app });
  });

  SubscriptionServer.create(
    { schema, execute, subscribe },
    { server, path: apolloServer.graphqlPath }
  );
};
