import InputValidError from '@shared/error/InputValidError';
import Validator, { ValidationConfig } from '@shared/validator';
import { GraphQLContext } from '@type';

const validator = (config: ValidationConfig, ctx: GraphQLContext) => {
  Validator.useLang(ctx.i18n.language);
  const validator = new Validator(config.data, config.rules, {
    ...ctx.t(`validator:message`, {
      returnObjects: true
    }),
    ...(config.messages || {})
  });

  validator.setAttributeFormatter((attribute: string) => {
    const newAttribute = attribute
      .split('.')
      .filter((x) => isNaN(parseInt(x)))
      .join('.');

    return (
      config?.attributes?.[newAttribute] ||
      ctx.t(`validator:attribute.${newAttribute}`)
    );
  });

  const passes = () => true;

  const fails = () => {
    throw new InputValidError(undefined, validator.errors.errors);
  };

  validator.checkAsync(passes, fails);
};

export default validator;
