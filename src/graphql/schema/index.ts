import authResolver from '@graphql/schema/auth';
import cloudinaryResolver from '@graphql/schema/cloudinary';
import { merge } from 'lodash';

export const resolvers = merge(authResolver, cloudinaryResolver);
