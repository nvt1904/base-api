import { MutationResolvers } from '@graphql/type';
import refreshTokenService from '@service/auth/refreshTokenService';
import { GraphQLContext } from '@type';

const refreshToken: MutationResolvers<GraphQLContext>['refreshToken'] = (
  _,
  { input }
) => {
  return refreshTokenService(input.refreshToken);
};

export default refreshToken;
