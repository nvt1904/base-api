import { QueryResolvers } from '@graphql/type';
import getProfileService from '@service/auth/getProfileService';
import { GraphQLContext } from '@type';

const profile: QueryResolvers<GraphQLContext>['profile'] = (_, __, ctx) => {
  return getProfileService(ctx.auth?.id);
};

export default profile;
