import { MutationResolvers } from '@graphql/type';
import updateProfileService, {
  DataUpdateProfile
} from '@service/auth/updateProfileService';
import { GraphQLContext } from '@type';

const updateProfile: MutationResolvers<GraphQLContext>['updateProfile'] =
  async (_, { input }, ctx) => {
    return updateProfileService(
      String(ctx.auth?.id),
      input as DataUpdateProfile
    );
  };

export default updateProfile;
