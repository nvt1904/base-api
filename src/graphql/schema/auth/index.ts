import { Resolvers } from '@graphql/type';
import { GraphQLContext } from '@type';
import login from './login';
import profile from './profile';
import refreshToken from './refreshToken';
import register from './register';
import updateProfile from './updateProfile';

const authResolver: Resolvers<GraphQLContext> = {
  Query: {
    profile
  },
  Mutation: {
    register,
    login,
    refreshToken,
    updateProfile
  }
};

export default authResolver;
