import { MutationResolvers } from '@graphql/type';
import validator from '@graphql/validator';
import loginService from '@service/auth/loginService';
import { GraphQLContext } from '@type';

const login: MutationResolvers<GraphQLContext>['login'] = async (
  _,
  { input },
  ctx
) => {
  validator(
    {
      data: input,
      rules: {
        email: 'required|email',
        password: 'required'
      }
    },
    ctx
  );
  return loginService(input);
};

export default login;
