import { MutationResolvers } from '@graphql/type';
import registerService from '@service/auth/registerService';
import { GraphQLContext } from '@type';

const register: MutationResolvers<GraphQLContext>['register'] = async (
  _,
  { input }
) => {
  return registerService(input);
};

export default register;
