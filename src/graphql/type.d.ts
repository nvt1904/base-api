import {
  GraphQLResolveInfo,
  GraphQLScalarType,
  GraphQLScalarTypeConfig
} from 'graphql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type RequireFields<T, K extends keyof T> = {
  [X in Exclude<keyof T, K>]?: T[X];
} & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  JSON: any;
  JWT: any;
  UUID: any;
};

export type LoginInput = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type LoginResult = {
  __typename?: 'LoginResult';
  accessToken?: Maybe<Scalars['JWT']>;
  expires?: Maybe<Scalars['DateTime']>;
  refreshToken?: Maybe<Scalars['JWT']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  login?: Maybe<LoginResult>;
  refreshToken?: Maybe<RefreshTokenResult>;
  register?: Maybe<RegisterResult>;
  updateProfile?: Maybe<Scalars['Boolean']>;
};

export type MutationLoginArgs = {
  input: LoginInput;
};

export type MutationRefreshTokenArgs = {
  input: RefreshTokenInput;
};

export type MutationRegisterArgs = {
  input: RegisterInput;
};

export type MutationUpdateProfileArgs = {
  input: UpdateProfileInput;
};

export type ProfileResult = {
  __typename?: 'ProfileResult';
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['UUID']>;
  name?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  profile?: Maybe<ProfileResult>;
  signUrlUploadTemp?: Maybe<SignUrlUploadTempResult>;
};

export type QuerySignUrlUploadTempArgs = {
  input?: InputMaybe<SignUrlUploadTempInput>;
};

export type RefreshTokenInput = {
  refreshToken: Scalars['JWT'];
};

export type RefreshTokenResult = {
  __typename?: 'RefreshTokenResult';
  accessToken?: Maybe<Scalars['JSON']>;
  expires?: Maybe<Scalars['DateTime']>;
};

export type RegisterInput = {
  email: Scalars['String'];
  name: Scalars['String'];
  password: Scalars['String'];
};

export type RegisterResult = {
  __typename?: 'RegisterResult';
  id?: Maybe<Scalars['UUID']>;
};

export type SignUrlUploadTempInput = {
  name?: InputMaybe<Scalars['String']>;
};

export type SignUrlUploadTempResult = {
  __typename?: 'SignUrlUploadTempResult';
  api_key?: Maybe<Scalars['String']>;
  public_id?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['String']>;
};

export type UpdateProfileInput = {
  avatar?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> {
  subscribe: SubscriptionSubscribeFn<
    { [key in TKey]: TResult },
    TParent,
    TContext,
    TArgs
  >;
  resolve?: SubscriptionResolveFn<
    TResult,
    { [key in TKey]: TResult },
    TContext,
    TArgs
  >;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<
  TResult,
  TKey extends string,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((
      ...args: any[]
    ) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (
  obj: T,
  context: TContext,
  info: GraphQLResolveInfo
) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
  TResult = {},
  TParent = {},
  TContext = {},
  TArgs = {}
> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  DateTime: ResolverTypeWrapper<Scalars['DateTime']>;
  JSON: ResolverTypeWrapper<Scalars['JSON']>;
  JWT: ResolverTypeWrapper<Scalars['JWT']>;
  LoginInput: LoginInput;
  LoginResult: ResolverTypeWrapper<LoginResult>;
  Mutation: ResolverTypeWrapper<{}>;
  ProfileResult: ResolverTypeWrapper<ProfileResult>;
  Query: ResolverTypeWrapper<{}>;
  RefreshTokenInput: RefreshTokenInput;
  RefreshTokenResult: ResolverTypeWrapper<RefreshTokenResult>;
  RegisterInput: RegisterInput;
  RegisterResult: ResolverTypeWrapper<RegisterResult>;
  SignUrlUploadTempInput: SignUrlUploadTempInput;
  SignUrlUploadTempResult: ResolverTypeWrapper<SignUrlUploadTempResult>;
  String: ResolverTypeWrapper<Scalars['String']>;
  UUID: ResolverTypeWrapper<Scalars['UUID']>;
  UpdateProfileInput: UpdateProfileInput;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Boolean: Scalars['Boolean'];
  DateTime: Scalars['DateTime'];
  JSON: Scalars['JSON'];
  JWT: Scalars['JWT'];
  LoginInput: LoginInput;
  LoginResult: LoginResult;
  Mutation: {};
  ProfileResult: ProfileResult;
  Query: {};
  RefreshTokenInput: RefreshTokenInput;
  RefreshTokenResult: RefreshTokenResult;
  RegisterInput: RegisterInput;
  RegisterResult: RegisterResult;
  SignUrlUploadTempInput: SignUrlUploadTempInput;
  SignUrlUploadTempResult: SignUrlUploadTempResult;
  String: Scalars['String'];
  UUID: Scalars['UUID'];
  UpdateProfileInput: UpdateProfileInput;
};

export interface DateTimeScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['DateTime'], any> {
  name: 'DateTime';
}

export interface JsonScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export interface JwtScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['JWT'], any> {
  name: 'JWT';
}

export type LoginResultResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['LoginResult'] = ResolversParentTypes['LoginResult']
> = {
  accessToken?: Resolver<Maybe<ResolversTypes['JWT']>, ParentType, ContextType>;
  expires?: Resolver<
    Maybe<ResolversTypes['DateTime']>,
    ParentType,
    ContextType
  >;
  refreshToken?: Resolver<
    Maybe<ResolversTypes['JWT']>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type MutationResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']
> = {
  login?: Resolver<
    Maybe<ResolversTypes['LoginResult']>,
    ParentType,
    ContextType,
    RequireFields<MutationLoginArgs, 'input'>
  >;
  refreshToken?: Resolver<
    Maybe<ResolversTypes['RefreshTokenResult']>,
    ParentType,
    ContextType,
    RequireFields<MutationRefreshTokenArgs, 'input'>
  >;
  register?: Resolver<
    Maybe<ResolversTypes['RegisterResult']>,
    ParentType,
    ContextType,
    RequireFields<MutationRegisterArgs, 'input'>
  >;
  updateProfile?: Resolver<
    Maybe<ResolversTypes['Boolean']>,
    ParentType,
    ContextType,
    RequireFields<MutationUpdateProfileArgs, 'input'>
  >;
};

export type ProfileResultResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['ProfileResult'] = ResolversParentTypes['ProfileResult']
> = {
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['UUID']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']
> = {
  profile?: Resolver<
    Maybe<ResolversTypes['ProfileResult']>,
    ParentType,
    ContextType
  >;
  signUrlUploadTemp?: Resolver<
    Maybe<ResolversTypes['SignUrlUploadTempResult']>,
    ParentType,
    ContextType,
    RequireFields<QuerySignUrlUploadTempArgs, never>
  >;
};

export type RefreshTokenResultResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['RefreshTokenResult'] = ResolversParentTypes['RefreshTokenResult']
> = {
  accessToken?: Resolver<
    Maybe<ResolversTypes['JSON']>,
    ParentType,
    ContextType
  >;
  expires?: Resolver<
    Maybe<ResolversTypes['DateTime']>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type RegisterResultResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['RegisterResult'] = ResolversParentTypes['RegisterResult']
> = {
  id?: Resolver<Maybe<ResolversTypes['UUID']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SignUrlUploadTempResultResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['SignUrlUploadTempResult'] = ResolversParentTypes['SignUrlUploadTempResult']
> = {
  api_key?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  public_id?: Resolver<
    Maybe<ResolversTypes['String']>,
    ParentType,
    ContextType
  >;
  timestamp?: Resolver<
    Maybe<ResolversTypes['String']>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface UuidScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['UUID'], any> {
  name: 'UUID';
}

export type Resolvers<ContextType = any> = {
  DateTime?: GraphQLScalarType;
  JSON?: GraphQLScalarType;
  JWT?: GraphQLScalarType;
  LoginResult?: LoginResultResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  ProfileResult?: ProfileResultResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  RefreshTokenResult?: RefreshTokenResultResolvers<ContextType>;
  RegisterResult?: RegisterResultResolvers<ContextType>;
  SignUrlUploadTempResult?: SignUrlUploadTempResultResolvers<ContextType>;
  UUID?: GraphQLScalarType;
};
