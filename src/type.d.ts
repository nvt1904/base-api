import { PrismaClient } from '@prisma/client';
import { RedisAdapter } from '@socket.io/redis-adapter';
import { Request } from 'express';
import { i18n, TFunction } from 'i18next';

export type DataResponse =
  | ({
      message?: string;
      error?: string;
    } & Record<string, unknown>)
  | string
  | undefined;
declare global {
  namespace NodeJS {
    interface Global {
      prisma: PrismaClient;
    }
  }

  namespace Express {
    interface Request {
      _id: string;
      resTemp: DataResponse;
      i18n: i18n;
      t: TFunction;
      auth?: Payload;
    }

    interface Response {
      jsonApi: (status?: number, data?: DataResponse) => Response;
      i18n: i18n;
      t: TFunction;
    }
  }
}

declare module 'socket.io' {
  interface Socket {
    auth?: Payload;
  }
}

export interface GraphQLContext {
  req: Request;
  i18n: i18n;
  t: TFunction;
  prisma: PrismaClient;
  auth?: Payload;
}

export type Payload = {
  id: string;
  type: 'user' | 'admin';
};
