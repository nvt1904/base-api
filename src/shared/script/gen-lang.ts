import {
  objectMultiToOneDepth,
  objectOneToMultiDepth,
  sortObjectByKey,
  ObjectOneDepth
} from '@shared/helper';
import { exec } from 'child_process';
import 'dotenv/config';
import fs from 'fs-extra';
import path from 'path';

const baseLang = process.env.LOCALE_DEFAULT || 'vi';
const dir = path.resolve('./src/locale/lang');
const fileDefault = 'translation.json';
const folders = fs.readdirSync(dir).filter((langFolder) => {
  return fs.lstatSync(path.resolve(dir, langFolder)).isDirectory();
});

if (!folders.includes(baseLang)) {
  fs.mkdirSync(path.resolve(dir, baseLang));
  folders.unshift(baseLang);
}

console.log('Languages', folders);

let files: string[] = [];
folders.forEach((folder) => {
  const filesTemp = fs.readdirSync(path.resolve(dir, folder)).filter((file) => {
    return fs.lstatSync(path.resolve(dir, folder, file)).isFile();
  });
  files = [...files, ...filesTemp]
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort();
});
console.log('Files ', files);

if (!files.length) {
  files.push(fileDefault);
}

files.forEach((file) => {
  let baseContentJson: ObjectOneDepth = {};
  try {
    console.log(`Rewrite ${baseLang}/${file}`);
    if (fs.existsSync(path.resolve(dir, baseLang, file))) {
      const contentText = fs.readFileSync(
        path.resolve(dir, baseLang, file),
        'utf-8'
      );
      baseContentJson = JSON.parse(contentText);
    } else {
      fs.writeFileSync(path.resolve(dir, baseLang, file), '{}');
    }
    baseContentJson = objectMultiToOneDepth(baseContentJson);
    fs.writeFileSync(
      path.resolve(dir, baseLang, file),
      JSON.stringify(sortObjectByKey(objectOneToMultiDepth(baseContentJson)))
    );
    // eslint-disable-next-line no-empty
  } catch (error) {
    console.log(`Rewrite ${baseLang}/${file} fail`);
  }
  folders
    .filter((x) => x !== baseLang)
    .forEach((folder) => {
      try {
        console.log(`Sync ${folder}/${file}`);
        if (!fs.existsSync(path.resolve(dir, folder, file))) {
          fs.writeFileSync(path.resolve(dir, folder, file), '{}');
        }
        const contentText = fs.readFileSync(
          path.resolve(dir, folder, file),
          'utf-8'
        );
        let contentJsonTemp;
        if (contentText == '') {
          contentJsonTemp = {};
        } else {
          contentJsonTemp = JSON.parse(contentText);
        }
        contentJsonTemp = objectMultiToOneDepth(contentJsonTemp);

        let writeContentJson = { ...baseContentJson } as ObjectOneDepth;
        // eslint-disable-next-line guard-for-in
        for (const key in contentJsonTemp) {
          writeContentJson = {
            ...writeContentJson,
            [key]: contentJsonTemp[key]
          };
        }
        fs.writeFileSync(
          path.resolve(dir, folder, file),
          JSON.stringify(
            sortObjectByKey(objectOneToMultiDepth(writeContentJson))
          )
        );
        // eslint-disable-next-line no-empty
      } catch (error) {
        console.log(`Sync ${folder}/${file} fail`);
      }
    });
});

console.log('formatting...');
exec('yarn format:lang');
