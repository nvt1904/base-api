import { CronJob } from 'cron';
import cloudinary from 'cloudinary';
import { format, addDays } from 'date-fns';
import logger from '@shared/logger';

const deleteFileTempJob = new CronJob(
  '0 0 * * *',
  () => {
    const start = format(addDays(new Date(), -2), 'yyyyMMdd');
    cloudinary.v2.api
      .delete_resources_by_prefix('temp/', {
        start
      })
      .then((res) => logger.info(res))
      .catch((error) => {
        logger.error('[CronJob] deleteFileTempJob', error);
        cloudinary.v2.api.delete_resources_by_prefix('temp/', {
          start
        });
      });
  },
  null,
  true
);

export default deleteFileTempJob;
