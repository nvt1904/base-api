import deleteFileTempJob from '@shared/job/deleteFileTempJob';

const jobStart = () => {
  deleteFileTempJob.start();
};

export default jobStart;
