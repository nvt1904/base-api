import 'dotenv/config';
import 'reflect-metadata';

import httpServer from '@app';
import logger from '@shared/logger';
import cluster from 'cluster';
import os from 'os';

// Start the server
const name = String(process.env.NAME || 'BASE API');
const host = String(process.env.HOST || 'http://localhost');
const port = Number(process.env.PORT || 1904);
const numCPUs = os.cpus().length;

if (process.env.NODE_ENV === 'production') {
  if (cluster.isMaster) {
    for (let i = 0; i < numCPUs; i++) {
      // Create a worker
      cluster.fork();
    }
  } else {
    // Workers share the TCP connection in this server
    httpServer.listen(port, () => {
      logger.info(
        `[App] ✔ ${name} started on worker ${process.pid} ${host}:${port}`
      );
    });
  }
} else {
  httpServer.listen(port, () => {
    logger.info(`[App] ✔ ${name} started. Goto ${host}:${port}`);
  });
}
